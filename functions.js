hoisted();
// notHoisted();// Error


const notHoisted = function () {
    console.log('This function is not hoisted so we can use only it after declaration');
}

function hoisted() {
    console.log('This function is hoisted so we can use it before declaration');
}

const IIFE = (function () {
    console.log('immediately Invoked function Expressions only can be used with FE');
})();

const expressionAsCallback = function (item) {
    //only function expressions can be used as callbacks
    return item * 2;
}
var arr = [4, 9, 2, 5, 6, 3];
arr = arr.map(expressionAsCallback);
console.log(arr);

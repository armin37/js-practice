let students = (function () {
    var students = [];

    return function addStudent(newStudentName) {
        if (students.includes(newStudentName)) {
            return newStudentName;
        }
        students.push(newStudentName);
        return students;
    }
}());

students('Armin');
students('Maryam');
let st = students('Reza');
//does not contain Reza so returns All
console.log(st);


st = students('Reza');
//contains Reza so just returns Reza
console.log(st);

function add(a, b, ...c) {
    let ans = a + b;
    if (c.length) {
        ans = c.reduce((previous, current) => previous + current, ans);
    }
    return ans;
}

let ans = add(5, 7, 8, 9);
console.log(ans);

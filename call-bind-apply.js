var obj = {num: 3};

function add(number, ...arg) {
    let res = this.num + number;
    if (arg.length) {
        res = arg.reduce((pre, curr) => pre + curr, res);
    }
    return res;
}

// console.log(add(5)); //Nan Error
const res = add.call(obj, 5);
console.log(res);


//apply accepts argument of type array
const res2 = add.apply(obj, [5]);
console.log(res2);

// bind creates a function which can be invoked anytime later
const bind = add.bind(obj, 5, 6);
const resultBind = bind();
console.log(resultBind);


let car = {
    model: 'BMW',
    printModel: function () {
        console.log(this.model);
    }
};

const modelFunc = car.printModel;

setTimeout(modelFunc, 1000); //undefined
setTimeout(modelFunc.bind(car), 1000); //BMW

// Bottom string has index but is not iterable, so it's array like
const arrayLikeString = "Array";

console.log(arrayLikeString[1]); //r

console.log(Array.isArray(arrayLikeString)); //false

console.log(convertWithSpread(arrayLikeString)); //Array of characters

console.log(convertWithArrayFrom(arrayLikeString)); //Array of characters

console.log(convertWithObjectValues(arrayLikeString)); //Array of characters

// arrayLikeString.map(item => console.log(item)); //TypeError: arrayLikeString.map is not a function

function convertWithSpread(str) {
    return [...str];
}

function convertWithArrayFrom(str) {
    return Array.from(str);
}

function convertWithObjectValues(str) {
    return Object.values(str);
}
